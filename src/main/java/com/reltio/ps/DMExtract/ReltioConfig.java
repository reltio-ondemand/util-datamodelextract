package com.reltio.ps.DMExtract;

import java.util.List;

public class ReltioConfig { private String utilVersion;
  private String tenantID;
  private String username;
  private String description;
  private String inheritence;
  private String json;
  private Object document;
  private List<Entity> entities;
  private List<Interaction> interaction;
  private List<Relation> relations;
  private List<MatchGroup> matchGroups;
  
  public ReltioConfig() {}
  
  public String getTenantID() { return tenantID; }
  
  
  public List<Interaction> getInteraction() {
	return interaction;
}

public void setInteraction(List<Interaction> interaction) {
	this.interaction = interaction;
}

public void setTenantID(String tenantID) {
    this.tenantID = tenantID;
  }
  
  public String getUsername() { return username; }
  
  public void setUsername(String username) {
    this.username = username;
  }
  
  public List<Entity> getEntities() { return entities; }
  
  public void setEntities(List<Entity> entities) {
    this.entities = entities;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) { this.description = description; }
  
  public String getInheritence() {
    return inheritence;
  }
  
  public void setInheritence(String inheritence) { this.inheritence = inheritence; }
  
  public String getJson() {
    return json;
  }
  
  public void setJson(String json) { this.json = json; }
  
  public Object getDocument() {
    return document;
  }
  
  public void setDocument(Object document) { this.document = document; }
  
  public List<Relation> getRelations() {
    return relations;
  }
  
  public void setRelations(List<Relation> relations) { this.relations = relations; }
  
  public boolean checkEntityExists(String uri)
  {
    for (Entity e : entities) {
      if (e.getUri().equalsIgnoreCase(uri))
        return true;
    }
    return false;
  }
  
  public List<MatchGroup> getMatchGroups() { return matchGroups; }
  
  public void setMatchGroups(List<MatchGroup> matchGroups) {
    this.matchGroups = matchGroups;
  }
  
  public String getUtilVersion() { return utilVersion; }
  
  public void setUtilVersion(String utilVersion) {
    this.utilVersion = utilVersion;
  }
}
