package com.reltio.ps.DMExtract;

import java.util.List;

public class MatchGroup { private String uri;
  private String mergeType;
  private String crossTypeMatch;
  private String label;
  private AttrMapping documentComparator;
  private List<MatchRuleDef> matchRuleDefs;
  public MatchGroup() {}
  
  private class MatchRuleDef { String ruleDefType;
    List<MatchRuleDef> matchRuleDefs;
    
    private MatchRuleDef() {}
    
    public String getRuleDefType() { return ruleDefType; }
    
    public void setRuleDefType(String ruleDefType)
    {
      this.ruleDefType = ruleDefType;
    }
    
    public List<MatchRuleDef> getMatchRuleDefs() {
      return matchRuleDefs;
    }
    

    public void setMatchRuleDefs(List<MatchRuleDef> matchRuleDefs) { this.matchRuleDefs = matchRuleDefs; } }
  
  private class ExactDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private ExactDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    

    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) { AttrURIs = attrURIs; }
  }
  
  private class EqualsDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrValuePair> attrValuePairs;
    
    private EqualsDef() {
      super();
    }
    
    public List<MatchGroup.AttrValuePair> getAttrValuePairs()
    {
      return attrValuePairs;
    }
    

    public void setAttrValuePairs(List<MatchGroup.AttrValuePair> attrValuePairs) { this.attrValuePairs = attrValuePairs; }
  }
  
  private class FuzzyDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private FuzzyDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    

    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) { AttrURIs = attrURIs; }
  }
  
  private class ExactOrNullDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private ExactOrNullDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    

    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) { AttrURIs = attrURIs; } }
  
  private class ExactOrAllNullDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private ExactOrAllNullDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    

    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) { AttrURIs = attrURIs; }
  }
  
  private class NotEqualsRuleDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrValuePair> attrValuePairs;
    
    private NotEqualsRuleDef() { super(); }
    
    public List<MatchGroup.AttrValuePair> getAttrValuePairs()
    {
      return attrValuePairs;
    }
    

    public void setAttrValuePairs(List<MatchGroup.AttrValuePair> attrValuePairs) { this.attrValuePairs = attrValuePairs; } }
  
  private class IgnoreInTokenRuleDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private IgnoreInTokenRuleDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    

    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) { AttrURIs = attrURIs; } }
  
  private class NullValuesSpec extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrValuePair> attrValuePairs;
    
    private NullValuesSpec() { super(); }
    
    public List<MatchGroup.AttrValuePair> getAttrValuePairs()
    {
      return attrValuePairs;
    }
    

    public void setAttrValuePairs(List<MatchGroup.AttrValuePair> attrValuePairs) { this.attrValuePairs = attrValuePairs; } }
  
  private class MatchTokenSpec extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrMapping> mapping;
    
    private MatchTokenSpec() { super(); }
    
    public List<MatchGroup.AttrMapping> getMapping()
    {
      return mapping;
    }
    

    public void setMapping(List<MatchGroup.AttrMapping> mapping) { this.mapping = mapping; } }
  
  private class ComparatorClassSpec extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrMapping> mapping;
    
    private ComparatorClassSpec() { super(); }
    
    public List<MatchGroup.AttrMapping> getMapping()
    {
      return mapping;
    }
    

    public void setMapping(List<MatchGroup.AttrMapping> mapping) { this.mapping = mapping; }
  }
  
  private class AttrMapping {
    String attrURI;
    String className;
    List<MatchGroup.Parameter> parameters;
    
    private AttrMapping() {}
    
    public String getAttrURI() { return attrURI; }
    
    public void setAttrURI(String attrURI)
    {
      this.attrURI = attrURI;
    }
    
    public String getClassName() {
      return className;
    }
    
    public void setClassName(String className) {
      this.className = className;
    }
    
    public List<MatchGroup.Parameter> getParameters() {
      return parameters;
    }
    

    public void setParameters(List<MatchGroup.Parameter> parameters) { this.parameters = parameters; }
  }
  
  private class Parameter {
    String parameter;
    String value;
    
    private Parameter() {}
    
    public String getParameter() { return parameter; }
    
    public void setParameter(String parameter)
    {
      this.parameter = parameter;
    }
    
    public String getValue() {
      return value;
    }
    

    public void setValue(String value) { this.value = value; }
  }
  
  private class AttrValuePair {
    String attrURI;
    String value;
    
    private AttrValuePair() {}
    
    public String getAttrURI() { return attrURI; }
    
    public void setAttrURI(String attrURI)
    {
      this.attrURI = attrURI;
    }
    
    public String getValue() {
      return value;
    }
    

    public void setValue(String value) { this.value = value; }
  }
  
  private class AttrURIList {
    String[] attrURIs;
    
    private AttrURIList() {}
    
    public String[] getAttrURIs() { return attrURIs; }
    


    public void setAttrURIs(String[] attrURIs) { this.attrURIs = attrURIs; } }
  
  private class NotExactSameDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private NotExactSameDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    

    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) { AttrURIs = attrURIs; } }
  
  private class NotFuzzySameDef extends MatchGroup.MatchRuleDef { List<MatchGroup.AttrURIList> AttrURIs;
    
    private NotFuzzySameDef() { super(); }
    
    public List<MatchGroup.AttrURIList> getAttrURIs()
    {
      return AttrURIs;
    }
    
    public void setAttrURIs(List<MatchGroup.AttrURIList> attrURIs) {
      AttrURIs = attrURIs;
    }
  }
  
  public String getUri() {
    return uri;
  }
  
  public void setUri(String uri) {
    this.uri = uri;
  }
  
  public String getMergeType() {
    return mergeType;
  }
  
  public void setMergeType(String mergeType) {
    this.mergeType = mergeType;
  }
  
  public String getCrossTypeMatch() {
    return crossTypeMatch;
  }
  
  public void setCrossTypeMatch(String crossTypeMatch) {
    this.crossTypeMatch = crossTypeMatch;
  }
  
  public String getLabel() {
    return label;
  }
  
  public void setLabel(String label) {
    this.label = label;
  }
  
  public AttrMapping getDocumentComparator() {
    return documentComparator;
  }
  
  public void setDocumentComparator(AttrMapping documentComparator) {
    this.documentComparator = documentComparator;
  }
  
  public List<MatchRuleDef> getMatchRuleDefs() {
    return matchRuleDefs;
  }
  
  public void setMatchRuleDefs(List<MatchRuleDef> matchRuleDefs) {
    this.matchRuleDefs = matchRuleDefs;
  }
}
