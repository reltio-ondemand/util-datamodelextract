package com.reltio.ps.DMExtract;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Entity {
    private String name;
    private String uri;
    private String label;
    private String isAbstract;
    private String description;
    private String dataLabelPattern;
    private String secondaryLabelPattern;
    private List<Attribute> attributes;
    
    private static final Logger logger = LogManager.getLogger(ReltioConfigUtil.class.getName());

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public Entity() {}


    public static String callGetterDynamic(Object o, String methodName) {
        String s;
        java.lang.reflect.Method method;
        try {
            method = o.getClass().getMethod(methodName, new Class[0]);
        } catch (SecurityException e) {
            logger.error("callGetterDynamic: Security Exception in callMethod - " + e);
            return null;
        } catch (NoSuchMethodException e) {
        	logger.error("callGetterDynamic: No such method exception in callMethod - " + e);
            return null;
        }
        try {
            s = (String) method.invoke(o, new Object[0]);
        } catch (Exception e) {
        	logger.error("callGetterDynamic - invocation exception in callMethod - " + e);
            return null;
        }
        return s;
    }

    public Attribute lookupAttributeByURI(String uri) {
        for (Attribute a : attributes) {
            if (a.getUri().equalsIgnoreCase(uri))
                return a;
        }
        return null;
    }

    public Entity(String uri, String label, String description, String dataLabelPattern, String secondaryLabelPattern) {
        this.uri = uri;
        this.label = label;
        this.description = description;
        this.dataLabelPattern = dataLabelPattern;
        this.secondaryLabelPattern = secondaryLabelPattern;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataLabelPattern() {
        return this.dataLabelPattern;
    }

    public void setDataLabelPattern(String dataLabelPattern) {
        this.dataLabelPattern = dataLabelPattern;
    }

    public String getSecondaryLabelPattern() {
        return this.secondaryLabelPattern;
    }

    public void setSecondaryLabelPattern(String secondaryLabelPattern) {
        this.secondaryLabelPattern = secondaryLabelPattern;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsAbstract() {
        return this.isAbstract;
    }

    public void setIsAbstract(String isAbstract) {
        this.isAbstract = isAbstract;
    }
}
